let billAmount = 0;

const billsDue = []; 



// TOP TABLE FUNCTION
function publishBillsDue(){

// SUM OF TOTAL BILLS
	let sumOfTotalBills = 0;
	let sumOfCashFunds = 0
	billsDue.forEach(function(indivBills){
		sumOfTotalBills += indivBills.tBillAmount;
		sumOfCashFunds += indivBills.tAddFunds;
	});
	const totalBills = sumOfTotalBills;
	const totalBillsValue = document.getElementById('totalBills');
	totalBillsValue.textContent = totalBills;

//CASH FUND
	
	const totalCashFund = sumOfCashFunds;
	const totalCashFundValue = document.getElementById('cashFunds');
	totalCashFundValue.textContent = totalCashFund;

// BALANCE
	let sumOfTotalBalance = 0;
	const balance = document.getElementById('balance');
	totalbalance = totalCashFund - totalBills;
	balance.textContent = totalbalance;

// DATA OF BILLS
	billsDue.forEach(function(indivBills, indexNumber){
	const newRow = document.createElement('tr');

// DAYS

	const todayDate = new Date();
	const monthNames = ["January", "February", "March", "April", "May", "June",
 	"July", "August", "September", "October", "November", "December"
	];

	newRow.innerHTML=`<td>${monthNames[new Date(indivBills.tDueDate).getMonth()]}</td>
	<td>${indivBills.tCompanyName}</td>
	<td>${indivBills.tBillAmount}</td>
	<td>${indivBills.tDueDate}</td>
	<td>${indivBills.tAddFunds}</td>
	<td>${Math.round((new Date(indivBills.tDueDate) - todayDate)/(1000*60*60*24))}</td>
	<td>${indivBills.tCategory}</td>
	<td><button id="paidbtn" class="paidBtn">Paid</button></td>
	<td><button id="removebtn" class="removeBtn" data-id="${indexNumber}">Remove</button></td>
	<td id="textStatus"></td>`
	document.getElementById('allDue').appendChild(newRow);
	});

		
}


// TOP TABLE

const addBillBtn = document.getElementById('addBillBtn');

addBillBtn.addEventListener('click', function(){
	document.getElementById('allDue').innerHTML = "";

	const companyName = document.getElementById('companyName').value;
	const billAmount = document.getElementById('billAmount').value;
	const dueDate = document.getElementById('dueDate').value;
	const addFunds = document.getElementById('addFunds').value;
	const category = document.getElementById('category').value;
		
	const inputContainer = {
		tCompanyName: companyName,
		tBillAmount: parseFloat(billAmount),
		tDueDate: dueDate,
		tAddFunds: parseFloat(addFunds),
		tCategory: category,
	};


	if( companyName === "" || billAmount <= 0 || new Date(dueDate) < new Date() || addFunds < 0 || category ===  "Bills Category"){
		alert("Check your data.");
		return;

	} else {
	

	billsDue.push(inputContainer);
	publishBillsDue();
	}
});



// REMOVE ADD BUTTON
document.addEventListener('click', function(){


	if( event.target.classList.contains('removeBtn')){
		alert("This item will be deleted.")
		document.getElementById('allDue').innerHTML = "";
		const index = event.target.getAttribute('data-id');
		billsDue.splice(index, 1);
		publishBillsDue();

	}

	if( event.target.classList.contains('paidBtn')){
		alert("Thank you for your payment. Bayad ka na.")
		document.getElementById('allDue').innerHTML = "";
		const index = event.target.getAttribute('data-id');
		billsDue.splice(index, 1);
		publishBillsDue();
	}
})



